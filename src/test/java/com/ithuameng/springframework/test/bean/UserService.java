package com.ithuameng.springframework.test.bean;

import com.ithuameng.springframework.beans.BeansException;
import com.ithuameng.springframework.beans.factory.*;
import com.ithuameng.springframework.context.ApplicationContext;
import com.ithuameng.springframework.context.ApplicationContextAware;

/**
 * 模拟用户 Service层
 *
 * @author ithuameng
 * @since 2022-7-15
 */
public class UserService implements InitializingBean, DisposableBean,
        BeanNameAware, BeanClassLoaderAware, ApplicationContextAware, BeanFactoryAware {

    // 通过 Aware容器感知获取
    private ApplicationContext applicationContext;

    private BeanFactory beanFactory;

    // 通过依赖注入获取
    private String uId;

    private String company;

    private String location;

    private UserDao userDao;

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("执行: UserService.afterPropertiesSet");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("执行: UserService.destroy");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) throws BeansException {
        System.out.println("ClassLoader：" + classLoader);
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("Bean Name is：" + name);
    }

    public void queryUserInfo() {
        System.out.println("查询用户信息: " + userDao.queryUserName(uId) + ", " + company + ", " + location);
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }
}
