package com.ithuameng.springframework.test.bean;

import java.util.Random;

/**
 * 模拟用户 Service层
 *
 * @author ithuameng
 * @since 2022-7-15
 */
public class UserService6 implements IUserService {

    private String token;

    public String queryUserInfo() {
        try {
            Thread.sleep(new Random(1).nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "浪子花梦，100001，" + token;
    }

    public String register(String userName) {
        try {
            Thread.sleep(new Random(1).nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "注册用户：" + userName + " success！";
    }
}
