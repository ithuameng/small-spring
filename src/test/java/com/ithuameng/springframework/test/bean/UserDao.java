package com.ithuameng.springframework.test.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * 模拟用户 Dao层
 *
 * @author ithuameng
 * @since 2022-7-15
 */
public class UserDao {

    private static Map<String, String> hashMap = new HashMap<>();

    public void initDataMethod() {
        System.out.println("执行: init-method");
        hashMap.put("10001", "浪子花梦");
        hashMap.put("10002", "徐小天");
    }

    public void destroyDataMethod(){
        System.out.println("执行: destroy-method");
        hashMap.clear();
    }

    public String queryUserName(String uId) {
        return hashMap.get(uId);
    }
}
