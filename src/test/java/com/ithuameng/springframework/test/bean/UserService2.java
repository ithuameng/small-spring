package com.ithuameng.springframework.test.bean;

/**
 * 模拟用户 Service层
 *
 * @author ithuameng
 * @since 2022-7-15
 */
public class UserService2 {

    // 通过依赖注入获取
    private String uId;

    private String company;

    private String location;

    private IUserDao userDao;

    public void queryUserInfo() {
        System.out.println("查询用户信息: " + userDao.queryUserName(uId) + ", " + company + ", " + location);
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public IUserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(IUserDao userDao) {
        this.userDao = userDao;
    }
}
