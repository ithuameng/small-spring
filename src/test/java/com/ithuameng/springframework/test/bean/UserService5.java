package com.ithuameng.springframework.test.bean;

import com.ithuameng.springframework.beans.factory.annotation.Autowired;
import com.ithuameng.springframework.beans.factory.annotation.Value;
import com.ithuameng.springframework.stereotype.Component;

import java.util.Random;

/**
 * 模拟用户 Service层
 *
 * @author ithuameng
 * @since 2022-7-15
 */
@Component(value = "userService5")
public class UserService5 implements IUserService {

    @Value("${token}")
    private String token;

    @Autowired
    private UserDao2 userDao;

    public String queryUserInfo() {
        try {
            Thread.sleep(new Random(1).nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return userDao.queryUserName("10001") + "，" + token;
    }

    public String register(String userName) {
        try {
            Thread.sleep(new Random(1).nextInt(100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "注册用户：" + userName + " success！";
    }
}
