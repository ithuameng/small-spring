package com.ithuameng.springframework.test.bean;

import com.ithuameng.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 模拟用户 Dao层
 *
 * @author ithuameng
 * @since 2022-7-15
 */
@Component
public class UserDao2 {

    private static Map<String, String> hashMap = new HashMap<>();

    static {
        hashMap.put("10001", "浪子花梦");
        hashMap.put("10002", "徐小天");
    }

    public String queryUserName(String uId) {
        return hashMap.get(uId);
    }
}
