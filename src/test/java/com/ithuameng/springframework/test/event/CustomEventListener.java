package com.ithuameng.springframework.test.event;

import com.ithuameng.springframework.context.ApplicationListener;

import java.util.Date;

/**
 * 自定义事件监听器
 *
 * @author ithuameng
 * @since 2022-7-25
 */
public class CustomEventListener implements ApplicationListener<CustomEvent> {

    @Override
    public void onApplicationEvent(CustomEvent event) {
        System.out.println("收到: " + event.getSource() + "消息");
        System.out.println("时间: " + new Date());
        System.out.println("消息: " + event.getId() + ":" + event.getMessage());
    }
}
