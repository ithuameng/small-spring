package com.ithuameng.springframework.test.common;

import com.ithuameng.springframework.beans.BeansException;
import com.ithuameng.springframework.beans.PropertyValue;
import com.ithuameng.springframework.beans.PropertyValues;
import com.ithuameng.springframework.beans.factory.ConfigurableListableBeanFactory;
import com.ithuameng.springframework.beans.factory.config.BeanDefinition;
import com.ithuameng.springframework.beans.factory.config.BeanFactoryPostProcessor;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("userService");
        PropertyValues propertyValues = beanDefinition.getPropertyValues();
        propertyValues.addPropertyValue(new PropertyValue("company", "改为：字节跳动"));
    }
}
