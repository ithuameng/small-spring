package com.ithuameng.springframework.test.advice;

import com.ithuameng.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * 自定义环绕拦截
 *
 * @author ithuameng
 * @since 2022-7-27
 */
public class UserServiceBeforeAdvice implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("Before拦截方法：" + method.getName());
    }
}
