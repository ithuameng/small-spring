package com.ithuameng.springframework.test.circularReference;

import com.ithuameng.springframework.beans.factory.FactoryBean;

import java.lang.reflect.Proxy;

/**
 * 婆婆，代理了媳妇原来妈妈的职责的类
 */
public class HusbandMother implements FactoryBean<IMother> {

    @Override
    public IMother getObject() throws Exception {
        return (IMother) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{IMother.class}, (proxy, method, args) -> "婚后媳妇妈妈的职责被婆婆代理了！" + method.getName());
    }

    @Override
    public Class<?> getObjectType() {
        return null;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}