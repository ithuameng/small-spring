package com.ithuameng.springframework.test.circularReference;

import com.ithuameng.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * 切面
 */
public class SpouseAdvice implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("关怀小两口(切面)：" + method);
    }
}
