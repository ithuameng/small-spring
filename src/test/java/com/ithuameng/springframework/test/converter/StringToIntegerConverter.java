package com.ithuameng.springframework.test.converter;

import com.ithuameng.springframework.core.convert.converter.Converter;

/**
 * String --> Integer 类型转换器
 */
public class StringToIntegerConverter implements Converter<String, Integer> {

    @Override
    public Integer convert(String source) {
        return Integer.valueOf(source);
    }
}
