package com.ithuameng.springframework.test;

import cn.hutool.core.io.IoUtil;
import com.ithuameng.springframework.aop.AdvisedSupport;
import com.ithuameng.springframework.aop.TargetSource;
import com.ithuameng.springframework.aop.aspectj.AspectJExpressionPointcut;
import com.ithuameng.springframework.aop.framework.Cglib2AopProxy;
import com.ithuameng.springframework.aop.framework.JdkDynamicAopProxy;
import com.ithuameng.springframework.beans.PropertyValue;
import com.ithuameng.springframework.beans.PropertyValues;
import com.ithuameng.springframework.beans.factory.config.BeanDefinition;
import com.ithuameng.springframework.beans.factory.config.BeanReference;
import com.ithuameng.springframework.beans.factory.support.DefaultListableBeanFactory;
import com.ithuameng.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import com.ithuameng.springframework.context.support.ClassPathXmlApplicationContext;
import com.ithuameng.springframework.core.convert.converter.Converter;
import com.ithuameng.springframework.core.convert.support.StringToNumberConverterFactory;
import com.ithuameng.springframework.core.io.DefaultResourceLoader;
import com.ithuameng.springframework.core.io.Resource;
import com.ithuameng.springframework.core.io.ResourceLoader;
import com.ithuameng.springframework.test.bean.*;
import com.ithuameng.springframework.test.circularReference.Husband;
import com.ithuameng.springframework.test.circularReference.Wife;
import com.ithuameng.springframework.test.common.MyBeanFactoryPostProcessor;
import com.ithuameng.springframework.test.common.MyBeanPostProcessor;
import com.ithuameng.springframework.test.converter.StringToIntegerConverter;
import com.ithuameng.springframework.test.event.CustomEvent;
import com.ithuameng.springframework.test.interceptor.UserServiceInterceptor;
import org.junit.Before;
import org.junit.Test;
import org.openjdk.jol.info.ClassLayout;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;

import static com.ithuameng.springframework.core.io.ResourceLoader.CLASSPATH_URL_PREFIX;

public class ApiTest {

    /**
     * 测试代码注册Bean
     */
    @Test
    public void test_BeanFactory() {
        // 1.初始化BeanFactory
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

        // 2.UserDao 注册
        beanFactory.registerBeanDefinition("userDao", new BeanDefinition(UserDao.class));

        // 3.UserService 设置属性 uId / userDao
        PropertyValues propertyValues = new PropertyValues();
        propertyValues.addPropertyValue(new PropertyValue("uId", "10002"));
        propertyValues.addPropertyValue(new PropertyValue("userDao", new BeanReference("userDao")));

        // 4.UserService 注入 Bean, 注册 UserService
        beanFactory.registerBeanDefinition("userService", new BeanDefinition(UserService.class, propertyValues));

        // 5.获取 UserService Bean
        UserService userService = (UserService) beanFactory.getBean("userService");
        userService.queryUserInfo();
    }


    private ResourceLoader resourceLoader;

    @Before
    public void init() {
        resourceLoader = new DefaultResourceLoader();
    }

    /**
     * 测试资源加载
     *
     * @throws IOException IO异常
     */
    @Test
    public void test_resource() throws IOException {
        Resource resource = resourceLoader.getResource(CLASSPATH_URL_PREFIX + "important.properties");
        // Resource resource = resourceLoader.getResource("src/test/resources/important.properties");
        // Resource resource = resourceLoader.getResource("https://***/important.properties");
        InputStream inputStream = resource.getInputStream();
        String content = IoUtil.readUtf8(inputStream);
        System.out.println(content);
    }

    /**
     * 不使用应用上下文测试 BeanPostProcessor、BeanFactoryPostProcessor
     */
    @Test
    public void test_BeanFactoryPostProcessorAndBeanPostProcessor() {
        // 1.初始化 BeanFactory
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

        // 2. 读取配置文件&注册 Bean
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
        reader.loadBeanDefinitions(CLASSPATH_URL_PREFIX + "spring.xml");

        // 3. BeanDefinition 加载完成 & Bean 实例化之前，修改 BeanDefinition 的属性值
        MyBeanFactoryPostProcessor beanFactoryPostProcessor = new MyBeanFactoryPostProcessor();
        beanFactoryPostProcessor.postProcessBeanFactory(beanFactory);

        // 4. Bean 实例化之后，修改 Bean 属性信息
        MyBeanPostProcessor beanPostProcessor = new MyBeanPostProcessor();
        beanFactory.addBeanPostProcessor(beanPostProcessor);

        // 5. 获取 Bean 对象调用方法
        UserService userService = beanFactory.getBean("userService", UserService.class);

        userService.queryUserInfo();
    }

    /**
     * 使用应用上下文测试 BeanPostProcessor、BeanFactoryPostProcessor
     */
    @Test
    public void test_BeanFactoryPostProcessorAndBeanPostProcessor_xml() {
        // 1.初始化 BeanFactory
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-post-processor.xml");

        // 2.获取 Bean对象调用方法
        UserService userService = applicationContext.getBean("userService", UserService.class);
        userService.queryUserInfo();
    }

    /**
     * 测试初始化 init和销毁方法 destroy
     */
    @Test
    public void test_xml() {
        // 1.初始化 BeanFactory
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring.xml");

        // 注册虚拟机钩子
        applicationContext.registerShutdownHook();

        // 2.获取 Bean对象调用方法
        UserService userService = applicationContext.getBean("userService", UserService.class);
        System.out.println("ApplicationContextAware: " + userService.getApplicationContext());
        System.out.println("BeanFactoryAware: " + userService.getBeanFactory());
        userService.queryUserInfo();
    }

    /**
     * 测试单例 & 原型
     */
    @Test
    public void test_prototype() {
        // 1.初始化 BeanFactory
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-proxy.xml");

        // 注册虚拟机钩子
        applicationContext.registerShutdownHook();

        // 2.获取 Bean对象调用方法
        UserService2 userService01 = applicationContext.getBean("userService", UserService2.class);
        UserService2 userService02 = applicationContext.getBean("userService", UserService2.class);

        // 3. 配置 scope="prototype/singleton"
        System.out.println(userService01);
        System.out.println(userService02);

        // 4. 打印十六进制哈希
        System.out.println(userService01 + " 十六进制哈希: " + Integer.toHexString(userService01.hashCode()));
        System.out.println(ClassLayout.parseInstance(userService01).toPrintable());
    }

    /**
     * 测试代理对象 IUserDao
     */
    @Test
    public void test_factory_bean() {
        // 1.初始化 BeanFactory
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-proxy.xml");

        // 注册虚拟机钩子
        applicationContext.registerShutdownHook();

        // 2. 调用代理方法
        UserService2 userService = applicationContext.getBean("userService", UserService2.class);
        userService.queryUserInfo();
    }

    /**
     * 测试容器事件和事件监听器
     */
    @Test
    public void test_event() {
        // 1.初始化 BeanFactory
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-event.xml");

        // 注册虚拟机钩子
        applicationContext.registerShutdownHook();

        // 2.发送事件
        applicationContext.publishEvent(new CustomEvent(applicationContext, 1000001L, "发送成功了"));
    }

    /**
     * 测试 aop匹配验证
     */
    @Test
    public void test_match() throws NoSuchMethodException {
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut("execution(* com.ithuameng.springframework.test.bean.UserService.*(..))");
        Class<UserService> clazz = UserService.class;
        Method method = clazz.getDeclaredMethod("queryUserInfo");
        System.out.println(pointcut.matches(clazz));
        System.out.println(pointcut.matches(method, clazz));
    }

    /**
     * 测试类方法动态代理
     */
    @Test
    public void test_proxy_method() {

        // 目标对象
        IUserService userService = new UserService3();

        // 组装代理信息
        AdvisedSupport advisedSupport = new AdvisedSupport();
        advisedSupport.setTargetSource(new TargetSource(userService));
        advisedSupport.setMethodInterceptor(new UserServiceInterceptor());
        advisedSupport.setMethodMatcher(new AspectJExpressionPointcut("execution(* com.ithuameng.springframework.test.bean.IUserService.*(..))"));

        // 代理对象(JdkDynamicAopProxy)
        IUserService proxy_jdk = (IUserService) new JdkDynamicAopProxy(advisedSupport).getProxy();

        // 测试调用
        System.out.println("测试结果：" + proxy_jdk.queryUserInfo());
        System.out.println();

        // 代理对象(Cglib2AopProxy)
        IUserService proxy_cglib = (IUserService) new Cglib2AopProxy(advisedSupport).getProxy();

        // 测试调用
        System.out.println("测试结果：" + proxy_cglib.register("花花"));
    }

    /**
     * 测试 AOP
     */
    @Test
    public void test_aop() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-aop.xml");
        IUserService userService = applicationContext.getBean("userService", IUserService.class);
        System.out.println("测试结果：" + userService.queryUserInfo());
    }

    /**
     * 测试占位符
     */
    @Test
    public void test_property() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-property.xml");
        IUserService userService = applicationContext.getBean("userService", IUserService.class);
        System.out.println("测试结果：" + userService);
    }

    /**
     * 测试包扫描
     */
    @Test
    public void test_scan() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-scan.xml");
        IUserService userService = applicationContext.getBean("userService", IUserService.class);
        System.out.println("测试结果：" + userService.queryUserInfo());
    }

    /**
     * 测试包扫描 @Autowired、@Value
     */
    @Test
    public void test_scan_autowired() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-scan-autowired.xml");
        IUserService userService = applicationContext.getBean("userService5", IUserService.class);
        System.out.println("测试结果：" + userService.queryUserInfo());
    }

    /**
     * 测试 AOP动态代理属性自动注入
     */
    @Test
    public void test_auto_proxy() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-auto-proxy.xml");
        IUserService userService = applicationContext.getBean("userService", IUserService.class);
        System.out.println("测试结果：" + userService.queryUserInfo());
    }

    /**
     * 测试循环依赖和 AOP三级缓存
     * <p>
     * 老公和媳妇互相依赖、婆婆是一个模拟成代理妈妈职责、在加上一个切面来关心家庭生活
     */
    @Test
    public void test_circular_reference() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-circular-reference.xml");
        Husband husband = applicationContext.getBean("husband", Husband.class);
        Wife wife = applicationContext.getBean("wife", Wife.class);
        System.out.println("老公的媳妇：" + husband.queryWife());
        System.out.println("媳妇的老公：" + wife.queryHusband());
    }

    /**
     * 测试 StringToIntegerConverter
     * <p>
     * String --> Integer 类型转换器
     */
    @Test
    public void test_StringToIntegerConverter() {
        StringToIntegerConverter converter = new StringToIntegerConverter();
        Integer num = converter.convert("1234");
        System.out.println("测试结果：" + num);
    }

    /**
     * 测试 StringToNumberConverterFactory
     * <p>
     * 将 String转换为任何jdk标准的 Number实现
     */
    @Test
    public void test_StringToNumberConverterFactory() {
        StringToNumberConverterFactory converterFactory = new StringToNumberConverterFactory();
        Converter<String, Integer> stringToIntegerConverter = converterFactory.getConverter(Integer.class);
        System.out.println("测试结果：" + stringToIntegerConverter.convert("1234"));
        Converter<String, Long> stringToLongConverter = converterFactory.getConverter(Long.class);
        System.out.println("测试结果：" + stringToLongConverter.convert("1234"));
    }

    /**
     * 测试类型转换器
     * <p>
     * Husband >> String --> Date
     */
    @Test
    public void test_convert() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(CLASSPATH_URL_PREFIX + "spring-converter.xml");
        com.ithuameng.springframework.test.converter.Husband husband = applicationContext.getBean("husband", com.ithuameng.springframework.test.converter.Husband.class);
        System.out.println("测试结果：" + husband.toString());
    }
}
