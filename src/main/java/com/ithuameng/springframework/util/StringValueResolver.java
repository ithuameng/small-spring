package com.ithuameng.springframework.util;

/**
 * 解析字符串
 *
 * @author ithuameng
 * @date 2022-8-1
 */
public interface StringValueResolver {

    String resolveStringValue(String strVal);
}
