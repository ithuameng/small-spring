package com.ithuameng.springframework.core.io;

/**
 * 按照资源加载的不同方式，资源加载器可以集中处理
 *
 * @author ithuameng
 * @date 2022-7-18
 */
public interface ResourceLoader {

    String CLASSPATH_URL_PREFIX = "classpath:";

    /**
     * 根据资源地址获取资源
     *
     * @param location 资源地址
     * @return 资源
     */
    Resource getResource(String location);
}
