package com.ithuameng.springframework.core.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * 实现三种不同的流文件操作：classPath、FileSystem、URL
 *
 * @author ithuameng
 * @date 2022-7-18
 */
public interface Resource {

    /**
     * 获取资源文件流
     *
     * @return InputStream
     * @throws IOException IO异常
     */
    InputStream getInputStream() throws IOException;
}
