package com.ithuameng.springframework.core.convert.support;

import com.ithuameng.springframework.core.convert.converter.Converter;
import com.ithuameng.springframework.core.convert.converter.ConverterFactory;
import com.ithuameng.springframework.util.NumberUtils;
import com.sun.istack.internal.Nullable;

/**
 * 将 String转换为任何jdk标准的 Number实现
 *
 * @author ithuameng
 * @date 2022-8-5
 */
public class StringToNumberConverterFactory implements ConverterFactory<String, Number> {

    @Override
    public <T extends Number> Converter<String, T> getConverter(Class<T> targetType) {
        return new StringToNumber<>(targetType);
    }

    private static final class StringToNumber<T extends Number> implements Converter<String, T> {

        private final Class<T> targetType;

        public StringToNumber(Class<T> targetType) {
            this.targetType = targetType;
        }

        @Override
        @Nullable
        public T convert(String source) {
            if (source.isEmpty()) {
                return null;
            }
            return NumberUtils.parseNumber(source, this.targetType);
        }
    }
}
