package com.ithuameng.springframework.core.convert.converter;

/**
 * 类型转换处理接口
 *
 * @author ithuameng
 * @date 2022-8-5
 */
public interface Converter<S, T> {

    /**
     * 将类型为{@code S}的源对象转换为目标类型{@code T}
     */
    T convert(S source);
}
