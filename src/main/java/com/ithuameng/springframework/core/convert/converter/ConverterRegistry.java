package com.ithuameng.springframework.core.convert.converter;

/**
 * 类型转换注册接口
 *
 * @author ithuameng
 * @date 2022-8-5
 */
public interface ConverterRegistry {

    /**
     * 向此注册表添加普通转换器
     * <p>
     * 可转换源/目标类型对派生自Converter的参数化类型
     */
    void addConverter(Converter<?, ?> converter);

    /**
     * 向此注册表添加通用转换器
     */
    void addConverter(GenericConverter converter);

    /**
     * 向此注册表添加远程转换器工厂
     * <p>
     * 可转换源/目标类型对派生自ConverterFactory的参数化类型
     */
    void addConverterFactory(ConverterFactory<?, ?> converterFactory);
}
