package com.ithuameng.springframework.core.convert.converter;

/**
 * 类型转换工厂
 *
 * @author ithuameng
 * @date 2022-8-5
 */
public interface ConverterFactory<S, R> {

    /**
     * 让转换器将 S转换为目标类型 T，其中 T也是 R的一个实例
     */
    <T extends R> Converter<S, T> getConverter(Class<T> targetType);
}
