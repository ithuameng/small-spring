package com.ithuameng.springframework.core.convert;

import com.sun.istack.internal.Nullable;

/**
 * 类型转换抽象接口
 * <p>
 * 用于类型转换的服务接口。 这是转换系统的入口点
 * 调用{@link #convert(Object, Class)}来使用这个系统执行线程安全的类型转换
 *
 * @author ithuameng
 * @date 2022-8-5
 */
public interface ConversionService {

    /**
     * 如果{@code sourceType}的对象可以转换为{@code targetType}，则返回{@code true}
     */
    boolean canConvert(@Nullable Class<?> sourceType, Class<?> targetType);

    /**
     * 将给定的{@code源}转换为指定的{@code targetType}
     */
    <T> T convert(Object source, Class<T> targetType);
}
