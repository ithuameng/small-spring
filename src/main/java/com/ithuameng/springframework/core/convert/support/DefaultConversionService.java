package com.ithuameng.springframework.core.convert.support;

import com.ithuameng.springframework.core.convert.converter.ConverterRegistry;

/**
 * {@link GenericConversionService}的专门化，默认配置为适用于大多数环境的转换器
 *
 * @author ithuameng
 * @date 2022-8-5
 */
public class DefaultConversionService extends GenericConversionService {

    public DefaultConversionService() {
        addDefaultConverters(this);
    }

    public static void addDefaultConverters(ConverterRegistry converterRegistry) {
        // 添加各类类型转换工厂
        converterRegistry.addConverterFactory(new StringToNumberConverterFactory());
    }
}
