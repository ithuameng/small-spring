package com.ithuameng.springframework.stereotype;

import java.lang.annotation.*;

/**
 * 注入 Bean到 Spring IOC中，相当于配置文件中的 <bean id="" class=""/>
 *
 * @author ithuameng
 * @date 2022-7-29
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Component {

    String value() default "";
}
