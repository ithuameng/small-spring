package com.ithuameng.springframework.beans.factory;

/**
 * Bean 初始化接口
 *
 * @author ithuameng
 * @date 2022-7-21
 */
public interface InitializingBean {

    /**
     * Bean 处理了属性填充后调用
     */
    void afterPropertiesSet() throws Exception;
}
