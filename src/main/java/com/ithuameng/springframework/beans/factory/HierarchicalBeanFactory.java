package com.ithuameng.springframework.beans.factory;

/**
 * 扩展工厂的层次子接口
 *
 * @author ithuameng
 * @date 2022-7-19
 */
public interface HierarchicalBeanFactory extends BeanFactory {
}
