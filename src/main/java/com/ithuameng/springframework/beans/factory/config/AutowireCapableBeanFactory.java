package com.ithuameng.springframework.beans.factory.config;

import com.ithuameng.springframework.beans.BeansException;
import com.ithuameng.springframework.beans.factory.BeanFactory;

/**
 * 自动化处理 Bean 工厂配置的接口
 *
 * @author ithuameng
 * @date 2022-7-19
 */
public interface AutowireCapableBeanFactory extends BeanFactory {

    /**
     * 执行 BeanPostProcessors 接口实现类的 postProcessBeforeInitialization 方法
     */
    Object applyBeanPostProcessorsBeforeInitialization(Object existingBean, String beanName) throws BeansException;

    /**
     * 执行 BeanPostProcessors 接口实现类的 postProcessorsAfterInitialization 方法
     */
    Object applyBeanPostProcessorsAfterInitialization(Object existingBean, String beanName) throws BeansException;
}
