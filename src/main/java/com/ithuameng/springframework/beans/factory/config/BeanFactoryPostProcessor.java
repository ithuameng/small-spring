package com.ithuameng.springframework.beans.factory.config;

import com.ithuameng.springframework.beans.BeansException;
import com.ithuameng.springframework.beans.factory.ConfigurableListableBeanFactory;

/**
 * Spring 框架组建提供的容器扩展机制
 * 允许在 Bean 对象注册后但未实例化之前对 Bean 的定义信息 BeanDefinition 执行修改操作
 *
 * @author ithuameng
 * @date 2022-7-19
 */
public interface BeanFactoryPostProcessor {

    /**
     * 在所有的 BeanDefinition 加载完成后，实例化 Bean 对象之前，提供修改 BeanDefinition 属性的机制
     */
    void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException;
}
