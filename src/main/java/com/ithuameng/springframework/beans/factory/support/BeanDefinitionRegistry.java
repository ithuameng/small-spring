package com.ithuameng.springframework.beans.factory.support;

import com.ithuameng.springframework.beans.factory.config.BeanDefinition;

/**
 * Bean信息注册接口
 *
 * @author ithuameng
 * @date 2022-7-12
 */
public interface BeanDefinitionRegistry {

    /**
     * 注册 Bean信息
     *
     * @param beanName       bean名称
     * @param beanDefinition bean定义信息
     */
    void registerBeanDefinition(String beanName, BeanDefinition beanDefinition);

    /**
     * 判断是否包含指定名称的 BeanDefinition
     * @param beanName bean名称
     * @return true包含 false不包含
     */
    boolean containsBeanDefinition(String beanName);
}
