package com.ithuameng.springframework.beans.factory.config;

/**
 * Bean引用
 *
 * @author ithuameng
 * @date 2022-7-15
 */
public class BeanReference {

    private final String beanName;

    public BeanReference(String beanName) {
        this.beanName = beanName;
    }

    public String getBeanName() {
        return beanName;
    }
}
