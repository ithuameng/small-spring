package com.ithuameng.springframework.beans.factory;

import com.ithuameng.springframework.beans.BeansException;

/**
 * 工厂对象
 *
 * @author ithuameng
 * @date 2022-8-4
 */
public interface ObjectFactory<T> {

    T getObject() throws BeansException;
}
