package com.ithuameng.springframework.beans.factory.support;

import com.ithuameng.springframework.core.io.DefaultResourceLoader;
import com.ithuameng.springframework.core.io.ResourceLoader;

/**
 * Bean 定义读取抽象类实现
 *
 * @author ithuameng
 * @date 2022-7-18
 */
public abstract class AbstractBeanDefinitionReader implements BeanDefinitionReader {

    private final BeanDefinitionRegistry registry;

    private ResourceLoader resourceLoader;

    public AbstractBeanDefinitionReader(BeanDefinitionRegistry registry) {
        this(registry, new DefaultResourceLoader());
    }

    public AbstractBeanDefinitionReader(BeanDefinitionRegistry registry, ResourceLoader resourceLoader) {
        this.registry = registry;
        this.resourceLoader = resourceLoader;
    }

    @Override
    public BeanDefinitionRegistry getRegistry() {
        return registry;
    }

    @Override
    public ResourceLoader getResourceLoader() {
        return resourceLoader;
    }
}
