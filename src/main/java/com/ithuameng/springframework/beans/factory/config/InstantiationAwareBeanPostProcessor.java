package com.ithuameng.springframework.beans.factory.config;

import com.ithuameng.springframework.beans.BeansException;
import com.ithuameng.springframework.beans.PropertyValues;

/**
 * 在目标 bean实例化之前应用这个 BeanPostProcessor
 * 返回的 bean对象可能是要使用的代理，而不是目标 bean
 * 有效地抑制目标 bean的默认实例化
 *
 * @author ithuameng
 * @date 2022-7-27
 */
public interface InstantiationAwareBeanPostProcessor extends BeanPostProcessor {

    /**
     * 在 Bean 对象执行初始化方法之前，执行此方法
     */
    Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException;

    /**
     * 在 Bean 对象执行初始化方法之后，执行此方法
     */
    boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException;

    /**
     * 在 Bean 对象实例化完成后，设置属性操作之前执行此方法
     */
    PropertyValues postProcessPropertyValues(PropertyValues pvs, Object bean, String beanName) throws BeansException;

    /**
     * 在 Spring 中由 SmartInstantiationAwareBeanPostProcessor#getEarlyBeanReference 提供
     */
    default Object getEarlyBeanReference(Object bean, String beanName) {
        return bean;
    }
}
