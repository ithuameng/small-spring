package com.ithuameng.springframework.beans.factory;

/**
 * 标记类接口，实现该接口可以被 Spring容器感知
 *
 * @author ithuameng
 * @date 2022-7-22
 */
public interface Aware {
}
