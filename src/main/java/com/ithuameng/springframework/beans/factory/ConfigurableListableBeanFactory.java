package com.ithuameng.springframework.beans.factory;

import com.ithuameng.springframework.beans.BeansException;
import com.ithuameng.springframework.beans.factory.config.AutowireCapableBeanFactory;
import com.ithuameng.springframework.beans.factory.config.BeanDefinition;
import com.ithuameng.springframework.beans.factory.config.ConfigurableBeanFactory;

/**
 * 提供分析和修改 Bean 以及预先实例化的操作接口
 *
 * @author ithuameng
 * @date 2022-7-19
 */
public interface ConfigurableListableBeanFactory extends ListableBeanFactory, AutowireCapableBeanFactory, ConfigurableBeanFactory {

    /**
     * 获取 Bean定义信息
     */
    BeanDefinition getBeanDefinition(String beanName) throws BeansException;

    /**
     * 提前实例化 Bean对象
     */
    void preInstantiateSingletons() throws BeansException;
}
