package com.ithuameng.springframework.beans.factory;

import com.ithuameng.springframework.beans.BeansException;

import java.util.Map;

/**
 * 扩展 Bean 工厂接口的接口
 *
 * @author ithuameng
 * @date 2022-7-19
 */
public interface ListableBeanFactory extends BeanFactory {

    /**
     * 按照类型返回 Bean 实例
     */
    <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException;

    /**
     * 返回注册表中所有的 Bean名称
     */
    String[] getBeanDefinitionNames();
}
