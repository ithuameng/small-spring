package com.ithuameng.springframework.beans.factory;

import com.ithuameng.springframework.beans.BeansException;

/**
 * 容器感知类
 * <p>
 * 实现此接口，既能感知到所属的 ClassLoader
 *
 * @author ithuameng
 * @date 2022-7-22
 */
public interface BeanClassLoaderAware extends Aware {

    void setBeanClassLoader(ClassLoader classLoader) throws BeansException;
}
