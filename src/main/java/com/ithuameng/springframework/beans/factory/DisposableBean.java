package com.ithuameng.springframework.beans.factory;

/**
 * Bean 销毁接口
 *
 * @author ithuameng
 * @date 2022-7-21
 */
public interface DisposableBean {

    void destroy() throws Exception;
}
