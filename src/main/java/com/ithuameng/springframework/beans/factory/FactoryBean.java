package com.ithuameng.springframework.beans.factory;

/**
 * FactoryBean 用于对象代理
 * <p>
 * 参考 test_prototype测试方法
 *
 * @param <T>
 * @author ithuameng
 * @date 2022-7-22
 */
public interface FactoryBean<T> {

    /**
     * 获取对象
     */
    T getObject() throws Exception;

    /**
     * 获取对象类型
     */
    Class<?> getObjectType();

    /**
     * 是否是单例对象
     */
    boolean isSingleton();
}
