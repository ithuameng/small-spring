package com.ithuameng.springframework.beans.factory.support;

import com.ithuameng.springframework.beans.BeansException;
import com.ithuameng.springframework.core.io.Resource;
import com.ithuameng.springframework.core.io.ResourceLoader;

/**
 * Bean 定义读取接口
 * <p>
 * getRegistry()、getResourceLoader()，都是用于提供给后面三个方法的工具
 * 加载和注册，这两个方法的实现会包装到抽象类中，以免污染具体的接口实现方法
 *
 * @author ithuameng
 * @date 2022-7-18
 */
public interface BeanDefinitionReader {

    BeanDefinitionRegistry getRegistry();

    ResourceLoader getResourceLoader();

    void loadBeanDefinitions(Resource resource) throws BeansException;

    void loadBeanDefinitions(Resource... resources) throws BeansException;

    void loadBeanDefinitions(String location) throws BeansException;

    void loadBeanDefinitions(String... location) throws BeansException;
}
