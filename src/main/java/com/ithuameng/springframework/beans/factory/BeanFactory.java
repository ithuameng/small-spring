package com.ithuameng.springframework.beans.factory;

import com.ithuameng.springframework.beans.BeansException;

/**
 * 工厂获取 Bean接口
 *
 * @author ithuameng
 * @date 2022-7-12
 */
public interface BeanFactory {

    /**
     * 获取无参 Bean
     */
    Object getBean(String name) throws BeansException;

    /**
     * 获取有参 Bean
     */
    Object getBean(String name, Object... args) throws BeansException;

    /**
     * 获取指定名称  指定类型的 Bean
     */
    <T> T getBean(String name, Class<T> requiredType) throws BeansException;

    /**
     * 获取指定类型的 Bean
     */
    <T> T getBean(Class<T> requiredType) throws BeansException;

    /**
     * 是否包含名称为 name的 Bean
     */
    boolean containsBean(String name);
}
