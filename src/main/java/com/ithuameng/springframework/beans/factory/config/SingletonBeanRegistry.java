package com.ithuameng.springframework.beans.factory.config;

/**
 * 获取单例 Bean对象接口
 *
 * @author ithuameng
 * @date 2022-7-12
 */
public interface SingletonBeanRegistry {

    /**
     * 获取单例对象
     */
    Object getSingleton(String beanName);

    /**
     * 保存一个单例 Bean
     */
    void registerSingleton(String beanName, Object singletonObject);
}
