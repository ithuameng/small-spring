package com.ithuameng.springframework.beans.factory.annotation;

import java.lang.annotation.*;

/**
 * 与 Autowired 配合使用
 *
 * @author ithuameng
 * @date 2022-8-1
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Qualifier {

    String value() default "";
}
