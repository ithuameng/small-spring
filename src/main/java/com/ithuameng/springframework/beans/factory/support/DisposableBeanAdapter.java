package com.ithuameng.springframework.beans.factory.support;

import cn.hutool.core.util.StrUtil;
import com.ithuameng.springframework.beans.BeansException;
import com.ithuameng.springframework.beans.factory.config.BeanDefinition;
import com.ithuameng.springframework.beans.factory.DisposableBean;

import java.lang.reflect.Method;

/**
 * 销毁方法适配器
 *
 * @author ithuameng
 * @date 2022-7-21
 */
public class DisposableBeanAdapter implements DisposableBean {

    private final Object bean;

    private final String beanName;

    private String destroyMethodName;

    public DisposableBeanAdapter(Object bean, String beanName, BeanDefinition beanDefinition) {
        this.bean = bean;
        this.beanName = beanName;
        this.destroyMethodName = beanDefinition.getDestroyMethodName();
    }

    @Override
    public void destroy() throws Exception {
        // 1.bean实现接口 DisposableBean
        if (bean instanceof DisposableBean) {
            ((DisposableBean) bean).destroy();
        }

        // 2.配置信息 destroy-method {判断是为了避免二次执行销毁}
        if (StrUtil.isNotEmpty(destroyMethodName) && !(bean instanceof DisposableBean && "destroy".equals(this.destroyMethodName))) {
            Method initMethod = bean.getClass().getMethod(destroyMethodName);
            if (null == initMethod) {
                throw new BeansException("Could not find an destroy method named '" + destroyMethodName + "' on bean with name '" + beanName + "'");
            }
            initMethod.invoke(bean);
        }
    }
}
