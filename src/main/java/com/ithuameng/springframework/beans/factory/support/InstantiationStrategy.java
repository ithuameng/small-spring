package com.ithuameng.springframework.beans.factory.support;

import com.ithuameng.springframework.beans.BeansException;
import com.ithuameng.springframework.beans.factory.config.BeanDefinition;

import java.lang.reflect.Constructor;

/**
 * 实例化策略接口
 *
 * @author ithuameng
 * @date 2022-7-14
 */
public interface InstantiationStrategy {

    /**
     * Bean实例化
     *
     * @param beanDefinition bean定义信息
     * @param beanName       bean名称
     * @param ctor           构造器
     * @param args           构造器参数
     * @return bean实例化对象
     * @throws BeansException 自定义bean异常
     */
    Object instantiate(BeanDefinition beanDefinition, String beanName, Constructor ctor, Object[] args) throws BeansException;
}
