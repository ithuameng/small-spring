package com.ithuameng.springframework.beans.factory.config;

import com.ithuameng.springframework.beans.factory.HierarchicalBeanFactory;
import com.ithuameng.springframework.core.convert.ConversionService;
import com.ithuameng.springframework.util.StringValueResolver;
import com.sun.istack.internal.Nullable;

/**
 * 获取 BeanPostProcessor、BeanClassLoader 等的一个配置化接口
 *
 * @author ithuameng
 * @date 2022-7-19
 */
public interface ConfigurableBeanFactory extends HierarchicalBeanFactory, SingletonBeanRegistry {

    String SCOPE_SINGLETON = "singleton";

    String SCOPE_PROTOTYPE = "prototype";

    /**
     * 添加 BeanPostProcessor
     */
    void addBeanPostProcessor(BeanPostProcessor beanPostProcessor);

    /**
     * 销毁单例对象
     */
    void destroySingletons();

    /**
     * 为内嵌值(如注释属性)添加一个String解析器
     */
    void addEmbeddedValueResolver(StringValueResolver valueResolver);

    /**
     * 解析给定的嵌入值，例如注释属性
     */
    String resolveEmbeddedValue(String value);

    /**
     * 指定一个 Spring 3.0的 ConversionService用于转换属性值，作为 JavaBeans propertyeditor的替代方案
     */
    void setConversionService(ConversionService conversionService);

    /**
     * 如果有的话，返回相关的 ConversionService
     */
    @Nullable
    ConversionService getConversionService();
}
