package com.ithuameng.springframework.beans.factory.config;

import com.ithuameng.springframework.beans.BeansException;

/**
 * Spring 提供的扩展机制
 * 在 Bean 对象实例化之后修改 Bean 对象，也可以替换 Bean 对象
 * 这部分与后面要实现的 AOP 有着密切的关系
 *
 * @author ithuameng
 * @date 2022-7-19
 */
public interface BeanPostProcessor {

    /**
     * 在 Bean 对象执行初始化方法之前，执行此方法
     */
    Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException;

    /**
     * 在 Bean 对象执行初始化方法之后，执行此方法
     */
    Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException;
}
