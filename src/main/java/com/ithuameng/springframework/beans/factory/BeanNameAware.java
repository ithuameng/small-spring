package com.ithuameng.springframework.beans.factory;

/**
 * 容器感知类
 * <p>
 * 实现此接口，既能感知到所属的 BeanName
 *
 * @author ithuameng
 * @date 2022-7-22
 */
public interface BeanNameAware extends Aware {

    void setBeanName(String name);
}
