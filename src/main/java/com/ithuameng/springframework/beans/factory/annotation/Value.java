package com.ithuameng.springframework.beans.factory.annotation;

import java.lang.annotation.*;

/**
 * 注入属性自定义注解
 *
 * @author ithuameng
 * @date 2022-8-1
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Value {

    /**
     * 实际值的表达: "# {systemProperties.myProp}"
     */
    String value();
}
