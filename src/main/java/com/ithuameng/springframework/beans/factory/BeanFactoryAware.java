package com.ithuameng.springframework.beans.factory;

import com.ithuameng.springframework.beans.BeansException;

/**
 * 容器感知类
 * <p>
 * 实现此接口，既能感知到所属的 BeanFactory
 *
 * @author ithuameng
 * @date 2022-7-22
 */
public interface BeanFactoryAware extends Aware {

    void setBeanFactory(BeanFactory beanFactory) throws BeansException;
}
