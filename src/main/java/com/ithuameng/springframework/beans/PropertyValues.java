package com.ithuameng.springframework.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean属性集合类 : Bean对象可能有多个属性，需要定义一个集合包装
 *
 * @author ithuameng
 * @date 2022-7-15
 */
public class PropertyValues {

    // Bean对象属性集合
    private final List<PropertyValue> propertyValueList = new ArrayList<>();

    public void addPropertyValue(PropertyValue propertyValue) {
        this.propertyValueList.add(propertyValue);
    }

    public PropertyValue[] getPropertyValueList() {
        return this.propertyValueList.toArray(new PropertyValue[0]);
    }

    public PropertyValue getPropertyValue(String propertyName) {
        for (PropertyValue pv : propertyValueList) {
            if (pv.getName().equals(propertyName)) {
                return pv;
            }
        }
        return null;
    }
}
