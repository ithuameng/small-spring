package com.ithuameng.springframework.beans;

/**
 * 定义Bean异常
 *
 * @author ithuameng
 * @date 2022-7-12
 */
public class BeansException extends RuntimeException {

    public BeansException(String msg) {
        super(msg);
    }

    public BeansException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
