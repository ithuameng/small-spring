package com.ithuameng.springframework.aop;

import org.aopalliance.aop.Advice;

/**
 * Advisor 访问者定义
 *
 * @author ithuameng
 * @see org.aopalliance.intercept.MethodInterceptor
 * @see BeforeAdvice
 * @date 2022-7-27
 */
public interface Advisor {

    /**
     * advice可以是拦截器(interceptor)、before advice、throws advice等
     */
    Advice getAdvice();
}
