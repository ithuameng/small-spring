package com.ithuameng.springframework.aop;

import org.aopalliance.aop.Advice;

/**
 * Advice 拦截器链接口定义
 *
 * @author ithuameng
 * @date 2022-7-27
 */
public interface BeforeAdvice extends Advice {
}
