package com.ithuameng.springframework.aop;

/**
 * 切点表达式
 *
 * @author ithuameng
 * @date 2022-7-26
 */
public interface Pointcut {

    ClassFilter getClassFilter();

    MethodMatcher getMethodMatcher();
}
