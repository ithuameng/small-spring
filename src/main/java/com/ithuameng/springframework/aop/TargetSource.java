package com.ithuameng.springframework.aop;

import com.ithuameng.springframework.util.ClassUtils;

/**
 * 目标对象
 *
 * @author ithuameng
 * @date 2022-7-26
 */
public class TargetSource {

    private final Object target;

    public TargetSource(Object target) {
        this.target = target;
    }

    public Class<?>[] getTargetClass(){
        Class<?> clazz = this.target.getClass();
        clazz = ClassUtils.isCglibProxyClass(clazz) ? clazz.getSuperclass() : clazz;
        return clazz.getInterfaces();
    }

    public Object getTarget(){
        return this.target;
    }
}
