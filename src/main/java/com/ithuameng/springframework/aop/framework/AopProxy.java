package com.ithuameng.springframework.aop.framework;

/**
 * 代理对象接口
 *
 * @author ithuameng
 * @date 2022-7-26
 */
public interface AopProxy {

    Object getProxy();
}
