package com.ithuameng.springframework.aop.framework;

import com.ithuameng.springframework.aop.AdvisedSupport;

/**
 * 代理工厂
 * <p>
 * 要解决的是关于 JDK 和 Cglib 两种代理的选择问题
 *
 * @author ithuameng
 * @date 2022-7-27
 */
public class ProxyFactory {

    private AdvisedSupport advisedSupport;

    public ProxyFactory(AdvisedSupport advisedSupport) {
        this.advisedSupport = advisedSupport;
    }

    public Object getProxy() {
        return createAopProxy().getProxy();
    }

    private AopProxy createAopProxy() {
        if (advisedSupport.isProxyTargetClass()) {
            return new Cglib2AopProxy(advisedSupport);
        }
        return new JdkDynamicAopProxy(advisedSupport);
    }
}
