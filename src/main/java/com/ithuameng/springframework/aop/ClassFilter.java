package com.ithuameng.springframework.aop;

/**
 * 类匹配类，用于切点找到给定的接口和目标类
 *
 * @author ithuameng
 * @date 2022-7-26
 */
public interface ClassFilter {

    /**
     * 切入点是否应该应用于给定的接口或目标类?
     */
    boolean matches(Class<?> clazz);
}
