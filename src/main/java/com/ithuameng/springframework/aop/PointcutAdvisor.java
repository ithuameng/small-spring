package com.ithuameng.springframework.aop;

/**
 * Advisor 承担了 Pointcut 和 Advice 的组合
 * Pointcut 用于获取 JoinPoint
 * 而 Advice 决定于 JoinPoint 执行什么操作
 *
 * @author ithuameng
 * @date 2022-7-27
 */
public interface PointcutAdvisor extends Advisor {

    /**
     * 获得切入点
     */
    Pointcut getPointcut();
}
