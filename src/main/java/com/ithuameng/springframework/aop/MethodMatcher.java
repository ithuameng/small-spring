package com.ithuameng.springframework.aop;

import java.lang.reflect.Method;

/**
 * 方法匹配，找到表达式范围内匹配下的目标类和方法
 *
 * @author ithuameng
 * @date 2022-7-26
 */
public interface MethodMatcher {

    /**
     * 静态检查给定的方法是否匹配
     */
    boolean matches(Method method, Class<?> targetClass);
}
