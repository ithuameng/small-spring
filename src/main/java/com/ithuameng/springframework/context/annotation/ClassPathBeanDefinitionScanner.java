package com.ithuameng.springframework.context.annotation;

import cn.hutool.core.util.StrUtil;
import com.ithuameng.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import com.ithuameng.springframework.beans.factory.config.BeanDefinition;
import com.ithuameng.springframework.beans.factory.support.BeanDefinitionRegistry;
import com.ithuameng.springframework.stereotype.Component;

import java.util.Set;

/**
 * 继承自 ClassPathScanningCandidateComponentProvider 的具体扫描包处理的类
 *
 * @author ithuameng
 * @date 2022-7-29
 */
public class ClassPathBeanDefinitionScanner extends ClassPathScanningCandidateComponentProvider {

    private BeanDefinitionRegistry registry;

    public ClassPathBeanDefinitionScanner(BeanDefinitionRegistry registry) {
        this.registry = registry;
    }

    public void doScan(String... basePackages) {
        for (String basePackage : basePackages) {
            Set<BeanDefinition> candidates = findCandidateComponents(basePackage);
            for (BeanDefinition beanDefinition : candidates) {
                // 解析 Bean的作用域 singleton、prototype
                String beanScope = resolveBeanScope(beanDefinition);
                if (StrUtil.isNotEmpty(beanScope)) {
                    beanDefinition.setScope(beanScope);
                }
                // 注册 Bean信息
                registry.registerBeanDefinition(determineBeanName(beanDefinition), beanDefinition);
            }
        }
        // 注册处理注解的 BeanPostProcessor (@Autowired、@Value)
        registry.registerBeanDefinition("com.ithuameng.springframework.context.annotation.internalAutowiredAnnotationProcessor", new BeanDefinition(AutowiredAnnotationBeanPostProcessor.class));
    }

    /**
     * 解析 Bean的作用域 singleton、prototype
     */
    private String resolveBeanScope(BeanDefinition beanDefinition) {
        Class<?> beanClass = beanDefinition.getBeanClass();
        Scope scope = beanClass.getAnnotation(Scope.class);
        if (null != scope) {
            return scope.value();
        }
        return StrUtil.EMPTY;
    }

    /**
     * 获取 BeanName名称
     * <p>
     * 若 @Component 没有指定 value，则 BeanName = 类名首字母小写
     */
    private String determineBeanName(BeanDefinition beanDefinition) {
        Class<?> beanClass = beanDefinition.getBeanClass();
        Component component = beanClass.getAnnotation(Component.class);
        String value = component.value();
        if (StrUtil.isEmpty(value)) {
            value = StrUtil.lowerFirst(beanClass.getSimpleName());
        }
        return value;
    }
}
