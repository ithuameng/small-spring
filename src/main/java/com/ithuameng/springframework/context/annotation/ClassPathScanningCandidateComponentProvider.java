package com.ithuameng.springframework.context.annotation;

import cn.hutool.core.util.ClassUtil;
import com.ithuameng.springframework.beans.factory.config.BeanDefinition;
import com.ithuameng.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 处理对象扫描装配
 *
 * @author ithuameng
 * @date 2022-7-29
 */
public class ClassPathScanningCandidateComponentProvider {

    /**
     * 这里先要提供一个可以通过配置路径 basePackage=com.ithuameng.springframework.test.bean
     * 通过这个方法就可以扫描到所有 @Component 注解的 Bean 对象了
     */
    public Set<BeanDefinition> findCandidateComponents(String basePackage) {
        Set<BeanDefinition> candidates = new LinkedHashSet<>();
        Set<Class<?>> classes = ClassUtil.scanPackageByAnnotation(basePackage, Component.class);
        for (Class<?> clazz : classes) {
            candidates.add(new BeanDefinition(clazz));
        }
        return candidates;
    }
}
