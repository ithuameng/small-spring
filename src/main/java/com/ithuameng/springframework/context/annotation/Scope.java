package com.ithuameng.springframework.context.annotation;

import java.lang.annotation.*;

/**
 * 用于配置作用域的自定义注解
 * <p>
 * 方便通过配置 Bean 对象注解的时候，拿到 Bean对象的作用域。不过一般都使用默认的 singleton
 *
 * @author ithuameng
 * @date 2022-7-29
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Scope {

    String value() default "singleton";
}
