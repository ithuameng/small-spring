package com.ithuameng.springframework.context.support;

import com.ithuameng.springframework.beans.factory.support.DefaultListableBeanFactory;
import com.ithuameng.springframework.beans.factory.xml.XmlBeanDefinitionReader;

/**
 * 上下文中对配置信息的加载
 *
 * @author ithuameng
 * @date 2022-7-19
 */
public abstract class AbstractXmlApplicationContext extends AbstractRefreshableApplicationContext {

    @Override
    protected void loadBeanDefinitions(DefaultListableBeanFactory beanFactory) {
        // 使用 XmlBeanDefinitionReader 类，处理了关于 XML 文件配置信息的操作
        XmlBeanDefinitionReader beanDefinitionReader = new XmlBeanDefinitionReader(beanFactory, this);
        String[] configLocations = getConfigLocations();
        if (null != configLocations) {
            beanDefinitionReader.loadBeanDefinitions(configLocations);
        }
    }

    /**
     * 为了从入口上下文类，拿到配置信息的地址描述
     */
    protected abstract String[] getConfigLocations();
}
