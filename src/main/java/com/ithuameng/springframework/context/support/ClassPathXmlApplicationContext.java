package com.ithuameng.springframework.context.support;

/**
 * 应用上下文实现类
 * <p>
 * 具体对外给用户提供的应用上下文方法，主要是对继承抽象类中方法的调用和提供了配置文件地址信息
 *
 * @author ithuameng
 * @date 2022-7-20
 */
public class ClassPathXmlApplicationContext extends AbstractXmlApplicationContext {

    private String[] configLocations;

    public ClassPathXmlApplicationContext() {
    }

    /**
     * 从 xml 中加载 beandefinition，并刷新上下文
     */
    public ClassPathXmlApplicationContext(String configLocation) {
        this(new String[]{configLocation});
    }

    /**
     * 从 XML 中加载 BeanDefinition，并刷新上下文
     */
    public ClassPathXmlApplicationContext(String[] configLocations) {
        this.configLocations = configLocations;
        refresh();
    }

    @Override
    protected String[] getConfigLocations() {
        return configLocations;
    }
}
