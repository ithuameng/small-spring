package com.ithuameng.springframework.context;

import com.ithuameng.springframework.beans.factory.ListableBeanFactory;

/**
 * 应用上下文接口
 * <p>
 * 继承于 ListableBeanFactory
 * 继承了关于 BeanFactory方法，比如一些 getBean 的方法
 *
 * @author ithuameng
 * @date 2022-7-19
 */
public interface ApplicationContext extends ListableBeanFactory, ApplicationEventPublisher {
}
