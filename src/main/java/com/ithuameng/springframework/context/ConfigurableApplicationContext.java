package com.ithuameng.springframework.context;

import com.ithuameng.springframework.beans.BeansException;

/**
 * 配置上下文接口
 * <p>
 * 继承自 ApplicationContext，并提供了 refresh 这个核心方法
 * 接下来也是需要在上下文的实现中完成刷新容器的操作过程
 *
 * @author ithuameng
 * @date 2022-7-19
 */
public interface ConfigurableApplicationContext extends ApplicationContext {

    /**
     * 刷新容器
     */
    void refresh() throws BeansException;

    /**
     * 注册虚拟机钩子
     */
    void registerShutdownHook();

    /**
     * 手动执行关闭
     */
    void close();
}
