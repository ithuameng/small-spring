package com.ithuameng.springframework.context;

import com.ithuameng.springframework.beans.BeansException;
import com.ithuameng.springframework.beans.factory.Aware;

/**
 * 容器感知类
 * <p>
 * 实现此接口，既能感知到所属的 ApplicationContext
 *
 * @author ithuameng
 * @date 2022-7-22
 */
public interface ApplicationContextAware extends Aware {

    void setApplicationContext(ApplicationContext applicationContext) throws BeansException;
}
