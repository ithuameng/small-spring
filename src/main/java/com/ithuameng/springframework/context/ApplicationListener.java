package com.ithuameng.springframework.context;

import java.util.EventListener;

/**
 * 应用程序事件监听器
 * <p>
 * 观察者设计模式(观察某一个事件被触发) -- 观察者
 *
 * @param <E> 继承了 ApplicationEvent的类
 * @author ithuameng
 * @date 2022-7-25
 */
public interface ApplicationListener<E extends ApplicationEvent> extends EventListener {

    /**
     * 处理应用程序事件
     */
    void onApplicationEvent(E event);
}
