package com.ithuameng.springframework.context;

import java.util.EventObject;

/**
 * 应用程序事件
 *
 * @author ithuameng
 * @date 2022-7-25
 */
public abstract class ApplicationEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public ApplicationEvent(Object source) {
        super(source);
    }
}
