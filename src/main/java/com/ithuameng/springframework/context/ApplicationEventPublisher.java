package com.ithuameng.springframework.context;

/**
 * 事件发布者
 * <p>
 * 整个一个事件的发布接口，所有的事件都需要从这个接口发布出去
 *
 * @author ithuameng
 * @date 2022-7-25
 */
public interface ApplicationEventPublisher {

    /**
     * 将应用程序事件通知向此应用程序注册的所有侦听器
     * 事件可以是框架事件(如 requestthandledevent)或应用程序特定的事件
     */
    void publishEvent(ApplicationEvent event);
}
