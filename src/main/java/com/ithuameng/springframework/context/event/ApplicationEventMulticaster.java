package com.ithuameng.springframework.context.event;

import com.ithuameng.springframework.context.ApplicationEvent;
import com.ithuameng.springframework.context.ApplicationListener;

/**
 * 事件广播器 -- 被观察者
 *
 * @author ithuameng
 * @date 2022-7-25
 */
public interface ApplicationEventMulticaster {

    /**
     * 添加用于通知所有事件的侦听器
     */
    void addApplicationListener(ApplicationListener<?> listener);

    /**
     * 从通知列表中删除侦听器
     */
    void removeApplicationListener(ApplicationListener<?> listener);

    /**
     * 将给定的应用程序事件多播到适当的侦听器
     */
    void multicastEvent(ApplicationEvent event);
}
