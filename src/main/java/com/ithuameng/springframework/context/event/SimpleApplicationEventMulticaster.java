package com.ithuameng.springframework.context.event;

import com.ithuameng.springframework.beans.factory.BeanFactory;
import com.ithuameng.springframework.context.ApplicationEvent;
import com.ithuameng.springframework.context.ApplicationListener;

/**
 * 事件广播器
 *
 * @author ithuameng
 * @date 2022-7-25
 */
public class SimpleApplicationEventMulticaster extends AbstractApplicationEventMulticaster {

    public SimpleApplicationEventMulticaster(BeanFactory beanFactory) {
        setBeanFactory(beanFactory);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void multicastEvent(ApplicationEvent event) {
        for (final ApplicationListener listener : getApplicationListeners(event)) {
            listener.onApplicationEvent(event);
        }
    }
}
