package com.ithuameng.springframework.context.event;

/**
 * 关闭事件
 *
 * @author ithuameng
 * @date 2022-7-25
 */
public class ContextClosedEvent extends ApplicationContextEvent {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public ContextClosedEvent(Object source) {
        super(source);
    }
}
